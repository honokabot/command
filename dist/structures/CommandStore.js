"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CommandStore = void 0;
class CommandStore {
    constructor() {
        this.commands = [];
    }
    getCommand(name) {
        const result = this.commands.find((entry) => entry.name === name);
        if (result === undefined) {
            return this.commands.find((entry) => entry.aliases.includes(name));
        }
        return result;
    }
    getCommands() {
        return this.commands;
    }
    registerCommand(command) {
        this.commands.push(command);
    }
    registerCommands(...commands) {
        commands.forEach((entry) => this.registerCommand(entry));
    }
    async executeCommand(name, message, args) {
        const result = this.getCommand(name)?.run(message, args);
        if (result === undefined) {
            throw new Error('The command does not exist.');
        }
    }
}
exports.CommandStore = CommandStore;
