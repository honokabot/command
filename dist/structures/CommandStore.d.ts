import { Message } from 'discord.js';
import { Command } from './Command';
export declare class CommandStore {
    private commands;
    constructor();
    getCommand(name: string): Command | undefined;
    getCommands(): Command[];
    registerCommand(command: Command): void;
    registerCommands(...commands: Command[]): void;
    executeCommand(name: string, message: Message, args: string[]): Promise<void>;
}
