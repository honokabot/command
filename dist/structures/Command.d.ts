import { Message } from 'discord.js';
export interface CommandOptions {
    name: string;
    description: string;
    usage: string;
    aliases: string[];
}
export declare abstract class Command {
    name: string;
    description: string;
    usage: string;
    aliases: string[];
    constructor(options: CommandOptions);
}
export interface Command {
    run(message: Message, args: string[]): Promise<void>;
    init?(): Promise<void>;
}
