"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Command = exports.CommandStore = void 0;
var CommandStore_1 = require("./structures/CommandStore");
Object.defineProperty(exports, "CommandStore", { enumerable: true, get: function () { return CommandStore_1.CommandStore; } });
var Command_1 = require("./structures/Command");
Object.defineProperty(exports, "Command", { enumerable: true, get: function () { return Command_1.Command; } });
