import { Message } from 'discord.js';
import { Command } from './Command';

export class CommandStore {
  private commands: Command[];

  constructor() {
    this.commands = [];
  }

  /**
   * @function getCommand
   * @description Get specified command
   * @param { String } name Command name
   */
  getCommand(name: string): Command | undefined {
    const result = this.commands.find((entry) => entry.name === name);
    if (result === undefined) {
      return this.commands.find((entry) => entry.aliases.includes(name));
    }
    return result;
  }

  /**
   * @function getCommands
   * @description Get registered commands
   */
  getCommands(): Command[] {
    return this.commands;
  }

  /**
   * @function registerCommand
   * @description Register a single command
   * @param { Command } command Command to register
   */
  registerCommand(command: Command): void {
    this.commands.push(command);
  }

  /**
   * @function registerCommands
   * @description Register multiple commands
   * @param { Command[] } commands Commands to register
   */
  registerCommands(...commands: Command[]): void {
    commands.forEach((entry) => this.registerCommand(entry));
  }

  /**
   *
   * @param { string } name Command name
   * @param { Message } message Message in Discord.js when executed
   * @param { string[] } args Arguments when the command is executed
   */
  async executeCommand(
    name: string,
    message: Message,
    args: string[]
  ): Promise<void> {
    const result = this.getCommand(name)?.run(message, args);
    if (result === undefined) {
      throw new Error('The command does not exist.');
    }
  }
}
