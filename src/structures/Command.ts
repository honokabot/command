import { Message } from 'discord.js';

export interface CommandOptions {
  name: string;
  description: string;
  usage: string;
  aliases: string[];
}

export abstract class Command {
  /**
   * The command Name
   */
  name: string;

  /**
   * The command description
   */
  description: string;

  /**
   * How to use this command
   */
  usage: string;

  /**
   * Alias of this command
   */
  aliases: string[];

  /**
   * @constructor
   * @param { CommandOptions } options
   */
  constructor(options: CommandOptions) {
    this.name = options.name;
    this.description = options.description;
    this.usage = options.usage;
    this.aliases = options.aliases;
  }
}

export interface Command {
  run(message: Message, args: string[]): Promise<void>;
  init?(): Promise<void>;
}
