# @honoka/command

The useful Discord.js command framework.

## Installation

With NPM:`npm i @honoka/command`
With Yarn:`yarn add @honoka/command`
